# cubroid_simple_audio_recognition

***

### 참고 사이트 
  1. https://www.isca-speech.org/archive/interspeech_2015/papers/i15_1478.pdf

***

##### 개발 언어
  - Python 3.6

##### 필요 패키지
  - pip install tensorflow

***

## 사용 예제

## 1. 학습 시키기


이미지 별로 모아서 폴더에 담습니다.
폴더명이 라벨이 됩니다. 직관적인 폴더명을 적어주세요.

![ex_screenshot](./images/folder.png)


"train.py" 파일은 딥러닝 학습을 시작하는 소스코드입니다. 

"models.py" 파일은 딥러닝 학습 알고리즘에 관한 코드입니다.

"input_data.py" 파일은 학습 데이터를 준비하는 코드입니다.


		python train.py --data_dir=./tmp/speech_dataset/ \
			--summaries_dir=./tmp/retrain_logs \ 
			--train_dir=./tmp/speech_commands_train \ 
			--wanted_words=yes,no,up,down,left,right,on,off,stop,go \ 
			--validation_percentage=10 \ 
			--testing_percentage=10


--data_dir
    = 데이터가 위치한 폴더.

--summaries_dir
	= tensorBoard의 요약 로그를 저장할 위치.

--train_dir
	= 이벤트 로그 및 체크포인트를 사용할 위치.

--wanted_words
	= 사용할 단어 (다른 단어는 알 수 없는 레이블에 추가됩니다.)

--validation_percentage
	= validation set으로 사용할 음원 데이터 비율.

--testing_percentage
	= test set으로 사용할 음원 데이터 비율.


#### 학습 완료 후 .pb(결과물 그래프파일-protobuf) 파일 생성하기


	python freeze.py \
		--start_checkpoint=./tmp/speech_commands_train/conv.ckpt-18000 \
		--output_file=./tmp/my_frozen_graph.pb


--start_checkpoint
	= 학습 최종 단계의 체크포인트 선택.

--output_file
	= 결과물 (.pb) 생성 위치.



## 2. 학습이 완료된 모델 사용

"label_wav.py"


	python label_wav.py \
		--graph=./tmp/my_frozen_graph.pb \
		--labels=./tmp/speech_commands_train/conv_labels.txt \
		--wav=./tmp/speech_dataset/left/left_0000000001.wav


--graph
	= 사용할 모델

--labels
	= 레이블 파일의 경로

--wav
	= 음원 분석할 음원 경로


# 결과

![ex_screenshot](./images/result.png)